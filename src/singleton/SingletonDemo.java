package singleton;

import org.apache.log4j.Logger;

public class SingletonDemo {
    private static final Logger LOGGER = Logger.getLogger(SingletonDemo.class);

    public static void main(String[] args) {
        LOGGER.info("SingletonDemo example started!");

        System.out.println("If you see the same value, then singleton was reused (yay!)" + "\n" +
                "If you see different values, then 2 singletons were created (booo!!)" + "\n\n" +
                "RESULT:" + "\n");

        Singleton singleton = Singleton.getInstance("FOO");
        Singleton anotherSingleton = Singleton.getInstance("BAR");

        System.out.println(singleton.value);
        System.out.println(anotherSingleton.value);
    }
}
