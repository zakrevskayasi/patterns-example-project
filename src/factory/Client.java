package factory;

import java.util.Scanner;

public class Client {
    public static void main(String[] args) {
        System.out.println("Please, enter desirable transport to deliver your product: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        ITransport transport = LogisticFactory.getTransportForDelivaring(name);
        transport.deliver();
    }
}
