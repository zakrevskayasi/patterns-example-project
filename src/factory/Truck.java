package factory;

public class Truck implements ITransport {
    @Override
    public void deliver() {
        System.out.println("Delivered by road in cargo truck!");
    }
}
