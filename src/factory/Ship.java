package factory;

public class Ship implements ITransport {
    @Override
    public void deliver() {
        System.out.println("Delivered by sea in cargo ship!");
    }
}
