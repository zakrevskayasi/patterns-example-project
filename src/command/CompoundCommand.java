package command;

import java.util.List;

public class CompoundCommand implements Command {
    private List<Command> commands;

    public CompoundCommand(List<Command> commands) {
        this.commands = commands;
    }

    @Override
    public void execute() {
        for (Command c : commands) {
            c.execute();
        }

    }
}
