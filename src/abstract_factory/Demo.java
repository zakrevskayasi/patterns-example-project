package abstract_factory;

import javafx.application.Application;

import java.util.Scanner;

public class Demo {
    private static Squadron getSquadron(String squadronName) {
        Squadron squadron = null;
        SquadronFactory squadronFactory;

        switch (squadronName) {
            case "elf":
                squadronFactory = new ElfSquadronFactory();
                squadron = new Squadron(squadronFactory);
                break;
            case "human":
                System.out.println("Human squadron!");
                break;
            case "ork":
                System.out.println("Ork squadron!");
                break;
            default:
                System.out.println("Unknown squadron!");
        }
        return squadron;
    }

    public static void main(String[] args) {
        System.out.println("Enter squadron name: ");
        Scanner sc = new Scanner(System.in);
        String name = sc.nextLine();

        Squadron squadron = getSquadron(name);
        squadron.fight();
    }
}
