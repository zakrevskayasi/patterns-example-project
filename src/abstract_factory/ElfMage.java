package abstract_factory;

public class ElfMage implements Mage {
    @Override
    public void cast() {
        System.out.println("Elf mage conjures!");
    }
}
