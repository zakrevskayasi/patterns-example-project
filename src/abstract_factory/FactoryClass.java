package abstract_factory;

import java.util.HashMap;
import java.util.Map;

public class FactoryClass {
    private static Map squadronMemebers = new HashMap<String, Object>();

    static Map createSquadron(SquadronFactory squadronFactory) {
        Mage mage = squadronFactory.createMage();
        Archer archer = squadronFactory.createArcher();
        Warrior warrior = squadronFactory.createWarrior();

        squadronMemebers.put("mage", mage);
        squadronMemebers.put("archer", archer);
        squadronMemebers.put("warrior", warrior);

        return squadronMemebers;
    }
}
