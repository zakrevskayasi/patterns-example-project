package builder;

public class CarDemo {
    public static void main(String[] args) {
        Car car = Car.newBuilder()
                .withEngine("petrol")
                .withSeats(4).build();
        System.out.println("Car 1: \n" + car.toString());

        Car car2 = Car.newBuilder()
                .withType("Hatchback")
                .withEngine("petrol")
                .withSeats(4)
                .withTransmission("Auto")
                .build();
        System.out.println("Car 2: \n" + car2.toString());



    }
}

